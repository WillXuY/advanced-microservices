/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.hellogradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloGradleApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloGradleApplication.class, args);
    }

    @GetMapping("/hello")
    public String helloGradle() {
        return "Hello,gradle!";
    }
}
