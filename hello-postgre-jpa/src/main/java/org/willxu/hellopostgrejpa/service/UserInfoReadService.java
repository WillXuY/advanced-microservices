/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 *
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.hellopostgrejpa.service;

import org.willxu.hellopostgrejpa.entity.UserInfo;

public interface UserInfoReadService {
    UserInfo getUserInfoById(Long userInfoId);
}
