/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 *
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.hellopostgrejpa.service.impl;

import org.springframework.stereotype.Service;
import org.willxu.hellopostgrejpa.entity.UserInfo;
import org.willxu.hellopostgrejpa.repository.UserInfoRepository;
import org.willxu.hellopostgrejpa.service.UserInfoReadService;

@Service
public class UserInfoReadServiceImpl implements UserInfoReadService {
    private final UserInfoRepository userInfoRepository;

    public UserInfoReadServiceImpl(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @Override
    public UserInfo getUserInfoById(Long userInfoId) {
        return userInfoRepository.getOne(userInfoId);
    }
}
