/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.helloh2jpa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.willxu.helloh2jpa.entity.UserInfo;
import org.willxu.helloh2jpa.repository.UserInfoRepository;
import org.willxu.helloh2jpa.service.UserInfoWriteService;

@Service
public class UserInfoWriteServiceImpl implements UserInfoWriteService {
    private final UserInfoRepository userInfoRepository;

    @Autowired
    public UserInfoWriteServiceImpl(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @Override
    public UserInfo addUser(UserInfo userInfo) {
        return userInfoRepository.save(userInfo);
    }
}
