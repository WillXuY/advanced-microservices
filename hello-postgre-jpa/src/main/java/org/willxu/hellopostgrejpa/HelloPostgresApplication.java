/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 *
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.hellopostgrejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class HelloPostgresApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloPostgresApplication.class, args);
    }

}
