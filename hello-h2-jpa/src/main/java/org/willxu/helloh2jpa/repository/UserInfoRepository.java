/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.helloh2jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.willxu.helloh2jpa.entity.UserInfo;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
}
